import { useEffect, useMemo, useState } from "react";

const Footer = () => {
  useEffect(() => {
    console.log("Footer");
  });

  const [n, setN] = useState(900000000);
  const sum = useMemo(() => Sum(n), [n]);
  const inc = () => setN(n + 1);
  const dec = () => setN(n - 1);

  return (
    <footer style={{ backgroundColor: "pink" }}>
      <h5>Footer</h5>
      <button onClick={dec}>-</button>
      1-den {n}-e qeder ededlerin cemi: {sum}
      <button onClick={inc}>+</button>
    </footer>
  );
};

const Sum = (n) => {
  console.log("Hesablanir...");
  return (n * (n + 1)) / 2;
};

export default Footer;
