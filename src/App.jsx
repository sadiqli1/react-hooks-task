import { useEffect, useReducer, useRef } from "react";
import Header from "./Header";
import Content from "./Content";
import Footer from "./Footer";
import ItemContext, { _items, _setItems } from "./Context";

const App = () => {
  useEffect(() => {
    console.log("App");
  });

  const [items, setItems] = useReducer(_setItems, _items);
  const ref = useRef();

  const add = () => {
    const { value } = ref.current;
    setItems({ name: "ADD", value });
    ref.current.value = "";
  };

  return (
    <ItemContext.Provider value={{ value: [items, setItems] }}>
      <section
        style={{ textAlign: "center", padding: 20, backgroundColor: "green" }}
      >
        <h5>App</h5>
        <Header />
        <input ref={ref} />
        <button onClick={add}>add</button>
        <hr />
        <Content />
        <hr />
        <Footer />
      </section>
    </ItemContext.Provider>
  );
};

export default App;
